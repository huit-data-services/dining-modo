import pymssql
import json
import secure

from icalendar import Calendar, Event
from datetime import datetime

class Dbflow:
    def __init__(self):
        self.server = secure.DB['server']
        self.port = secure.DB['port']
        self.db = secure.DB['db']
        self.user = secure.DB['user']
        self.password = secure.DB['password']
        self.conn = pymssql.connect(host=self.server, user=self.user, password=self.password, database=self.db, port=self.port)

    def locations(self):
        cursor = self.conn.cursor()
        cursor.execute("""
        select location_number, location_name, address, city, state, zip_code
        from Locations
        """)
        row = cursor.fetchone()
        locations = []
        while row:
            locations.append({'id': row[0],
                'name': row[1],
                'address': row[2],
                'city': row[3],
                'state': row[4],
                'zip': row[5]
            })
            row = cursor.fetchone()

        f = open('locations.json', 'w')
        json.dump(locations, f)

        return locations

    def recipes(self):
        cursor = self.conn.cursor()
        cursor.execute("""
        select
        ID,
        Serve_Date,
        Meal_Number,
        Meal_Name,
        Location_Number,
        Location_Name,
        Menu_Category_Number,
        Menu_Category_Name,
        Recipe_Number,
        Recipe_Name,
        Recipe_Print_As_Name,
        Ingredient_List,
        Allergens,
        Recipe_Print_As_Color,
        Recipe_Print_As_Character,
        Recipe_Product_Information,
        convert(varchar(30), Selling_Price, 1) as selling_price,
        convert(varchar(30), Portion_Cost, 1) as portion_cost,
        Production_Department,
        Service_Department,
        Catering_Department,
        Recipe_Web_Codes,
        Serving_Size,
        Calories,
        Calories_From_Fat,
        Total_Fat,
        Total_Fat_DV,
        Sat_Fat,
        Sat_Fat_DV,
        Trans_Fat,
        Trans_Fat_DV,
        Cholesterol,
        Cholesterol_DV,
        Sodium,
        Sodium_DV,
        Total_Carb,
        Total_Carb_DV,
        Dietary_Fiber,
        Dietary_Fiber_DV,
        Sugars,
        Sugars_DV,
        Protein,
        Protein_DV,
        Update_Date
        from ForecastedRecipes
        """)

        row = cursor.fetchone()
        recipes = []
        while row:
            recipes.append({
                'id': row[0],
                'serve_date': row[1],
                'meal_id': row[2],
                'meal_name': row[3],
                'location_id': row[4],
                'location_name': row[5],
                'menu_category_number': row[6],
                'menu_category_name': row[7],
                'recipe_number': row[8],
                'recipe_name': row[9],
                'recipe_print_name': row[10],
                'ingredient_list': row[11],
                'allergens': row[12],
                'recipe_print_color': row[13],
                'recipe_print_char': row[14],
                'recipe_info': row[15],
                'selling_price': row[16],
                'portion_cost': row[17],
                'production_department': row[18],
                'service_department': row[19],
                'catering_department': row[20],
                'recipe_web_codes': row[21],
                'serving_size': row[22],
                'calories': row[23],
                'calories_from_fat': row[24],
                'total_fat': row[25],
                'total_fat_dv': row[26],
                'sat_fat': row[27],
                'sat_fat_dv': row[28],
                'trans_fat': row[29],
                'trans_fat_dv': row[30],
                'cholesterol': row[31],
                'cholesterol_dv': row[32],
                'sodium': row[33],
                'sodium_dv': row[34],
                'total_carb': row[35],
                'total_carb_dv': row[36],
                'dietary_fiber': row[37],
                'dietary_fiber_dv': row[38],
                'sugars': row[39],
                'sugars_dv': row[40],
                'protein': row[41],
                'protein_dv': row[42],
                'update_date': row[43]
            })
            row = cursor.fetchone()

        f = open('recipes.json', 'w')
        json.dump(recipes, f)

        return recipes

    def events(self):
        cursor = self.conn.cursor()
        cursor.execute("""
        select meal_name,
            location_number,
            location_name,
            serve_date,
            meal_number
        from forecastedrecipes
        group by meal_name, location_number, location_name, serve_date, meal_number
        order by location_number;
        """)
        row = cursor.fetchone()
        events = []
        while row:
            events.append({
                'meal_name': row[0],
                'location_id': row[1],
                'location_name': row[2],
                'serve_date': row[3],
                'meal_id': row[4]
            })
            row = cursor.fetchone()
        f = open('modo-events.json', 'w')
        json.dump(events, f)
        return events

    def close(self):
        self.conn.close()

dbflow = Dbflow()
locations = dbflow.locations()
recipes = dbflow.recipes()
events = dbflow.events()

######################

class ModoFlow:
    def __init__(self):
        return
    def transformLocation(location):
        '''
        <eventsFeedConfig>
          <locationID>test1</locationID>
          <menuURL>http://example.com/dining/menus.php</menuURL>
          <menuItemURL>http://example.com/dining/menuItem.php</menuItemURL>
          <baseURL>kgo://data/dining/Franklin_Dining_Hall.ics</baseURL>
        </eventsFeedConfig>
        '''
        return

def createICSfile(location):
    cal = Calendar()
    for event in events:
        if event['location_id'] == location['id']:
            ievent = Event()
            starttime = datetime.strptime(event['serve_date'], '%m/%d/%Y')
            ievent['dtstart'] = starttime.strftime('%Y%m%dT%f')
            ievent['dtend'] = starttime.strftime('%Y%m%dT%f')
            ievent['mealid'] = event['location_id'] + '-' + str(event['meal_id'])
            ievent['uid'] = event['location_id'] + '-' + str(event['meal_id'])
            ievent['summary'] = event['meal_name']
            cal.add_component(ievent)
    ical = cal.to_ical()
    f = open('location_ics/' + location['name'].replace(" ", "_") + '.ics', 'wb')
    f.write(ical)
    f.close()

def transformLocation(location):
    '''
    <eventsFeedConfig>
      <locationID>test1</locationID>
      <menuURL>http://example.com/dining/menus.php</menuURL>
      <menuItemURL>http://example.com/dining/menuItem.php</menuItemURL>
      <baseURL>kgo://data/dining/Franklin_Dining_Hall.ics</baseURL>
    </eventsFeedConfig>
    '''
    location['mapName'] = location['name']
    createICSfile(location)
    location['eventsFeedConfig'] = {
        'locationID': location['id'],
        'menuURL': 'modo/menus',
        'menuItemURL': 'modo/menuItem',
        'baseURL': location['name'].replace(" ", "_") + '.ics'
    }
    return location
modoLocations = list(map(transformLocation, locations))
f = open('modo-locations.json', 'w')
json.dump(modoLocations, f)

def transformMenu(recipe):
    '''
    {
    "menus": [{
        "id": "test1",
        "name": "Test Item #1",
        "description": "Description of menu item"
    }, {
        "id": "test2",
        "name": "Test Item #2",
        "description": "Description of menu item"
    }]
    }
    '''
    menu = {};
    menu['id'] = recipe['id']
    menu['name'] = recipe['recipe_print_name']
    menu['description'] = recipe['ingredient_list']
    menu['event_id'] = recipe['meal_id']
    menu['location_id'] = recipe['location_id']
    return menu
modoMenus = {'menus': list(map(transformMenu, recipes))}
f = open('modo-menus.json', 'w')
json.dump(modoMenus, f)
