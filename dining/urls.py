"""dining URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns
from dining.api import views

# Wire up our API using automatic URL routing

urlpatterns = [
    url(r'^$', views.RootView.as_view()),
    url(r'^locations', views.LocationView.as_view(), name='locations'),
    url(r'^recipes', views.RecipeView.as_view(), name='recipes'),
    url(r'^modo/$', views.ModoRootView.as_view(), name='modo-root'),
    url(r'^modo/locations$', views.ModoLocationView.as_view(), name='modo-locations'),
    url(r'^modo/menus$', views.ModoMenuView.as_view(), name='modo-menus'),
    url(r'^modo/menuItems$', views.ModoMenuItemView.as_view(), name='modo-menu-items'),
    url(r'^modo/events$', views.ModoEventView.as_view(), name='modo-events'),

    url(r'^modo/locations/ics/(?P<icsFile>.+)$', views.ModoIcsView.as_view(), name='modo-ics')

]

# Additionally, we include login URLs for the browsable API
urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['xml', 'json', 'api', 'txt'])

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
