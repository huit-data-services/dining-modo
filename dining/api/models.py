from django.db import models
import json

class Location(object):
    def __init__(self, **kwargs):
        for field in ('id', 'name', 'city', 'state', 'address', 'zip'):
            setattr(self, field, kwargs.get(field, None))

'''
locations_data_file = open('locations.json')
locations_data = json.load(locations_data_file)
locations = {}
counter = 1
for location in locations_data:
    print(location)
    #locations[counter] = Location(location)
    counter += 1
print(locations)
'''
