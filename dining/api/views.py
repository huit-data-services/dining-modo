from django.contrib.auth.models import User, Group
from dining import settings, secure
from dining.api.models import Location
from rest_framework import viewsets, pagination
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework_xml.renderers import XMLRenderer
from dining.renderers import PlainTextRenderer
from rest_framework_xml.parsers import XMLParser
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework import mixins

import boto3

import urllib

from dining.api.serializers import UserSerializer, GroupSerializer, LocationSerializer

import json

# only used to fake out modo's caching...
from random import randint

def getS3(key, isJson=True):
    if 'access_id' in secure.S3:
        s3 = boto3.resource(service_name='s3', aws_access_key_id=secure.S3['access_id'], aws_secret_access_key=secure.S3['secret_key'], region_name=secure.S3['region'])
    else:
        s3 = boto3.resource(service_name='s3', region_name=secure.S3['region'])        
    raw_data = s3.Bucket(secure.S3['bucket']).Object(key).get()['Body'].read().decode("utf-8")
    if isJson:
        data = json.loads(raw_data)
    else:
        data = raw_data

    return data


class RootView(APIView):
    '''
    This is the root view for the dining services API.
    The locations and recipes routes come directly from the dining services database
    and contain almost everything.
    '''
    def get(self, request, format=None):
        return Response({
            'locations': reverse('locations', request=request, format=format),
            'recipes': reverse('recipes', request=request, format=format),
            'modo': reverse('modo-root', request=request, format=format)
        })

class LocationView(APIView):
    def get(self, request, format=None):
        #with open('locations.json') as data_file:
            #data = json.load(data_file)
        data = getS3('locations.json')
        return Response(data)

class RecipeView(APIView):
    '''
    This view is limited to 10 records by default.
    '''
    pagination_class = pagination.PageNumberPagination


    def get(self, request, format=None):
        limit = 10
        if 'count' in request.query_params:
            if request.query_params['limit'].isdigit():
                limit = int(request.query_params['limit'])
        #with open('recipes.json') as data_file:
        #    data = json.load(data_file)
        #    return Response(data[:limit])

        data = getS3('recipes.json')
        return Response(data[:limit])


######### MODO Specific ##############

class ModoRootView(APIView):
    def get(self, request, format=None):
        return Response({
            'locations': reverse('modo-locations', request=request, format=format),
            'menus': reverse('modo-menus', request=request, format=format),
            'events': reverse('modo-events', request=request, format=format)
        })

class ModoLocationView(APIView):
    #renderer_classes = (XMLRenderer, BrowsableAPIRenderer, JSONRenderer, )
    XMLRenderer.item_tag_name = 'location'
    XMLRenderer.root_tag_name = 'locations'
    def get(self, request, format=None):
        self.request = request
        self.format = format
        #with open('modo-locations.json') as data_file:
            #data = json.load(data_file)
        data = getS3('modo-locations.json')
        data = list(map(self.transformLocation, data))
        return Response(data)
    def transformLocation(self, location):
        location['eventsFeedConfig']['menuURL'] = self.request.build_absolute_uri(reverse('modo-menus', format=self.format))
        location['eventsFeedConfig']['menuItemURL'] = self.request.build_absolute_uri(reverse('modo-menu-items', format=self.format)) + "?" + str(randint(0,100000))
        location['eventsFeedConfig']['baseURL'] = self.request.build_absolute_uri(reverse('modo-ics', args=(location['eventsFeedConfig']['baseURL'], ), format=None))
        return location

class ModoMenuView(APIView):
    '''
    http://example.com/menus.php?locationID=&menuID=&eventID=4F402846-1BE3-4DB6-8014-F512C0907068%7C20141009T230000&startTime=1412827200&endTime=1412920800

    The menuID above will only be populated if a "menuid"
    attribute is included on ICS events. It is up to your
    script to interpret these parameters and return the correct
    menu data. The menu data will be displayed in a list for each event.

    NOTE: The 'eventID' attribute will be passed in the format 'id|timestamp'
    and your web service must be able to interpret this format in order to
    function properly.
    '''
    def get(self, request, format=None):
        #with open('modo-menus.json') as data_file:
            #data = json.load(data_file)
        data = getS3('modo-menus.json')
        if 'eventID' in request.query_params:
            decodedEventID = urllib.parse.unquote(request.query_params['eventID'])
            (locationIDandeventID, timestamp) = decodedEventID.split("|")
            (locationID, eventID, origtimestamp) = locationIDandeventID.split('-')
            data['menus'] = filter(lambda menu: str(menu['event_id']) == eventID and menu['location_id'] == locationID and menu['serve_date'] == origtimestamp, data['menus'])
        else:
            data['menus'] = data['menus'][:10]
        return Response(data)

class ModoMenuItemView(APIView):
    '''
    Example incoming string:
        /modo/menuItems?71564&locationID=27&menuID=&eventID=27-1%257C20170117T000000&startTime=1484629200&endTime=1484629200&menuItemID=31906536
    '''
    def get(self, request, format=None):
        #with open('modo-menus.json') as data_file:
            #data = [menu for menu in json.load(data_file)['menus'] if str(menu['id']) == request.query_params['menuItemID']][0]
        data = getS3('modo-menus.json')
        data = [menu for menu in data['menus'] if str(menu['id']) == request.query_params['menuItemID']][0]
        return Response(data)

class ModoEventView(APIView):
    def get(self, request, format=None):
        #with open('modo-events.json') as data_file:
        #    data = json.load(data_file)
        data = getS3('modo-events.json')
        return Response(data)

class ModoIcsView(APIView):
    renderer_classes = (PlainTextRenderer, BrowsableAPIRenderer, JSONRenderer, XMLRenderer, )
    def get(self, request, icsFile, format=None):
        '''
        localhost:8000/modo/locations/ics/Sebastian's_Cafe.ics
        '''
        #with open('location_ics/' + icsFile, 'r') as ics_file:
        #    ics_data = ics_file.read()
        ics_data = getS3('location_ics/' + icsFile, isJson=False)
        return Response(ics_data)
