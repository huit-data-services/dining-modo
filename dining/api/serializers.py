from django.contrib.auth.models import User, Group
from dining.api.models import Location
from rest_framework import serializers

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class LocationSerializer(serializers.Serializer):
    class Meta:
        model = Location
    id = serializers.CharField()
    name = serializers.CharField()
    address = serializers.CharField()
    zip = serializers.CharField()
    state = serializers.CharField()
