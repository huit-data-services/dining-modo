import smtplib
from email.mime.text import MIMEText
import pprint
import os

def send(subject, body):
    fromAddr = 'Dining API <jcleveng@fas.harvard.edu>'
    toAddr = 'jcleveng@fas.harvard.edu'
    # tacking on the env vars to the email for context
    body += "\n\n" + pprint.pformat(dict(os.environ))
    msg = MIMEText(body)

    msg['Subject'] = "Dining API: " + subject
    msg['From'] = fromAddr
    msg['To'] = toAddr

    s = smtplib.SMTP('smtp.fas.harvard.edu')
    s.sendmail(fromAddr, [toAddr], msg.as_string())
    s.quit()
