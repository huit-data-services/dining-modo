import json

from icalendar import Calendar, Event
from datetime import datetime
import re

from dining import settings
from dining import secure
from dining.flow import S3
from jsonschema import validate
from jsonschema.exceptions import ValidationError

import boto3

'''
Transforms the data from DBFlow into what modo wants
'''
class ModoFlow:
    def __init__(self, create_files=True):
        self.events = []
        self.create_files = create_files
        try:
            if 'access_id' in secure.S3:
                self.s3 = boto3.resource(service_name='s3', aws_access_key_id=secure.S3['access_id'], aws_secret_access_key=secure.S3['secret_key'], region_name=secure.S3['region'])
            else:
                self.s3 = boto3.resource(service_name='s3', region_name=secure.S3['region'])
        except Exception as e:
            Mail.send("S3 Error", "An error with the s3 resource: " + str(e))
            sys.exit(1)
        self.bucket = secure.S3['bucket']

        return

    def locations(self, locations):
        self.locations = self.transformLocations(locations)
        if self.create_files:
            self._createLocationsJson(self.locations)

    def setEvents(self, events):
        self.events = events

    def menus(self, recipes):
        self.menus = self.transformMenus(recipes)
        if self.create_files:
            self._createMenusJson(self.menus)

    def transformLocations(self, locations):
        '''
        This method just remaps a new location list to the format modo wants it
        <?xml version= "1.0"?>
        <locations>
          <location>
            <name>Franklin Dining Hall</name>
            <description>Franklin Dining Hall is the main campus dining hall for first year students. It opened in 1956 and has underwent a major renovation in 1993, adding additional kitchen prep areas, restrooms and student lounge areas.</description>
            <summary>The main campus dining hall for first year students</summary>
            <mapName>Franklin Dining Hall</mapName>
            <eventsFeedConfig>
              <locationID>test1</locationID>
              <menuURL>http://example.com/dining/menus.php</menuURL>
              <menuItemURL>http://example.com/dining/menuItem.php</menuItemURL>
              <baseURL>kgo://data/dining/Franklin_Dining_Hall.ics</baseURL>
            </eventsFeedConfig>
          </location>
        </locations>
        '''
        locations = list(map(self._transformLocation, locations))
        schemaFile = open(settings.BASE_DIR + '/dining/schemas/modo-locations.schema.json')
        schema = json.load(schemaFile)

        try:
            validate(locations, schema)
        except ValidationError as e:
            Mail.send("Invalid json", "JSON Generated (modo-locations) does not conform to the json schema: " + str(e) + '\n\n' + json.dumps(locations))
            sys.exit(1)

        return locations

    def _transformLocation(self, location):
        '''
        This is a map transformation to convert each individual location
            dict to the format modo needs.
        <eventsFeedConfig>
          <locationID>test1</locationID>
          <menuURL>http://example.com/dining/menus.php</menuURL>
          <menuItemURL>http://example.com/dining/menuItem.php</menuItemURL>
          <baseURL>kgo://data/dining/Franklin_Dining_Hall.ics</baseURL>
        </eventsFeedConfig>
        '''
        location['mapName'] = location['name']
        if self.create_files:
            self._createICSfile(location, self.events)
        location['eventsFeedConfig'] = {
            'locationID': location['id'],
            'menuURL': 'modo/menus',
            'menuItemURL': 'modo/menuItem',
            'baseURL': location['name'].replace(" ", "_") + '.ics'
        }

        return location

    def _createICSfile(self, location, events):
        '''
        This method creates the ics file using the events created from dbflow
        (This is literally the bullshit example ics code modo gives in its docs.)
BEGIN:VCALENDAR
PRODID:-//Google Inc//Google Calendar 70.9054//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-CALNAME:Name
X-WR-TIMEZONE:Time Zone
X-WR-CALDESC:Description
BEGIN:VTIMEZONE
TZID:America/Chicago
X-LIC-LOCATION:America/Chicago
BEGIN:DAYLIGHT
TZOFFSETFROM:-0600
TZOFFSETTO:-0500
TZNAME:CDT
DTSTART:19700308T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=2SU
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:-0500
TZOFFSETTO:-0600
TZNAME:CST
DTSTART:19701101T020000
RRULE:FREQ=YEARLY;BYMONTH=11;BYDAY=1SU
END:STANDARD
END:VTIMEZONE
BEGIN:VEVENT
DTSTART:20160510T160000Z
DTEND:20160510T190000Z
DTSTAMP:20160715T184337Z
UID:uid@google.com
CREATED:20160509T144945Z
DESCRIPTION:Teriyaki Steak\nVegetable Lo Mein\nSteamed Brown Rice\nBroccoli
 \n\nBaked Potato Bar
LAST-MODIFIED:20160509T174439Z
LOCATION:
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Description
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20160509T160000Z
DTEND:20160509T220000Z
DTSTAMP:20160715T184337Z
UID:uid@google.com
CREATED:20160509T144618Z
DESCRIPTION:Baked Chicken\nTofu Scampi\nCalifornia Vegetables\nHerbed Rice
 Pilaf\n\nBBQ Chicken Snack Wrap
LAST-MODIFIED:20160509T144756Z
LOCATION:
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:Description
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20160509T220000Z
DTEND:20160510T003000Z
DTSTAMP:20160715T184337Z
UID:uid@google.com
CREATED:20160509T144749Z
DESCRIPTION:Lemon Dill White Fish\nHot & Spicy Tofu\nLemon Rice Pilaf\nGree
 n Bean Sautee\n\nCheeseburger Snack Wrap
LAST-MODIFIED:20160509T144755Z
LOCATION:
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:Description
TRANSP:OPAQUE
END:VEVENT
END:VCALENDAR
        '''
        cal = Calendar()
        for event in events:
            if event['location_id'] == location['id']:
                ievent = Event()
                starttime = datetime.strptime(event['serve_date'], '%m/%d/%Y').strftime('%Y%m%d')
                if 'start_time' in event:
                    time = datetime.strptime(event['start_time'], '%I:%M%p')
                    seconds = '{0:02}'.format(time.hour) + '{0:02}'.format(time.minute) + '{0:02}'.format(time.second)
                    starttime += 'T' + seconds
                ievent['dtstart'] = starttime
                if 'end_time' in event:
                    endtime = datetime.strptime(event['serve_date'], '%m/%d/%Y').strftime('%Y%m%d')
                    time = time.strptime(event['end_time'], '%I:%M%p')
                    seconds = '{0:02}'.format(time.hour) + '{0:02}'.format(time.minute) + '{0:02}'.format(time.second)
                    ievent['dtend'] = endtime + 'T' + seconds
                ievent['mealid'] = event['location_id'] + '-' + str(event['menu_category_id']) + '-' + event['serve_date']
                ievent['uid'] = event['location_id'] + '-' + str(event['menu_category_id']) + '-' + event['serve_date']
                ievent['summary'] = event['menu_category_name']
                cal.add_component(ievent)
        ical = cal.to_ical()
        #f = open('location_ics/' + location['name'].replace(" ", "_") + '.ics', 'wb')
        #f.write(ical)
        #f.close()
        filename = 'location_ics/' + location['name'].replace(" ", "_") + '.ics'
        S3.backup(self.s3, filename, 'backup')

        self.s3.Bucket(self.bucket).put_object(Key=filename, Body=ical)


    def _createLocationsJson(self, modoLocations):
        #f = open('modo-locations.json', 'w')
        #json.dump(modoLocations, f)
        S3.backup(self.s3, 'modo-locations.json', 'backup')

        locationJson = json.dumps(modoLocations)
        self.s3.Bucket(self.bucket).put_object(Key='modo-locations.json', Body=locationJson)


    def transformMenus(self, recipes):
        menus = list(filter(lambda menu: menu['name'] != '', list(map(self._transformMenu, recipes))))
        schemaFile = open(settings.BASE_DIR + '/dining/schemas/modo-menus.schema.json')
        schema = json.load(schemaFile)

        try:
            validate(menus, schema)
        except ValidationError as e:
            Mail.send("Invalid json", "JSON Generated (modo-menus) does not conform to the json schema: " + str(e) + '\n\n' + json.dumps(menus))
            sys.exit(1)

        return { 'menus': menus }

    def _transformMenu(self, recipe):
        '''
        {
        "menus": [{
            "id": "test1",
            "name": "Test Item #1",
            "description": "Description of menu item"
        }, {
            "id": "test2",
            "name": "Test Item #2",
            "description": "Description of menu item"
        }]
        }
        '''
        menu = {};
        menu['id'] = recipe['id']
        menu['name'] = recipe['recipe_print_name']
        menu['description'] = recipe['recipe_info']
        menu['event_id'] = recipe['menu_category_number']
        menu['location_id'] = recipe['location_id']
        menu['serve_date'] = recipe['serve_date']
        menu['allergens'] = recipe['allergens']
        menu['ingredients'] = recipe['ingredient_list']
        classificationMap = {
            'CALC': 'calcium',
            'OG': 'organic',
            'CAL': 'calcium',
            'DFIB': '',
            'HFAT': 'healthy-fat',
            'HPRO': 'healthy-protein',
            'LOC': 'local',
            'OG': 'organic',
            'VGM': 'vegan',
            'VGN': 'vegan',
            'VGR': 'vegetarian',
            'VGT': 'vegetarian',
            'WGRN': 'whole-grain'
        }
        if re.match("^\w", recipe['recipe_web_codes']):
            classifications = []
            for web_code in recipe['recipe_web_codes'].split(" "):
                if web_code in classificationMap:
                    classifications.append(classificationMap[web_code])
            menu['classifications'] = classifications

        menu['nutrition'] = []
        for thing in ['serving_size', 'calories', 'calories_from_fat', 'total_carb', 'dietary_fiber', 'sugars', 'sodium', 'trans_fat', 'sat_fat', 'cholesterol']:
            name = ' '.join(thing.split("_")).title()
            value = recipe[thing]
            if value != '':
                menu['nutrition'].append({'name': name, 'value': value})

        return menu

    def _createMenusJson(self, modoMenus):
        #f = open('modo-menus.json', 'w')
        #json.dump(modoMenus, f)
        menuJson = json.dumps(modoMenus)
        S3.backup(self.s3, 'modo-menus.json', 'backup')

        self.s3.Bucket(self.bucket).put_object(Key='modo-menus.json', Body=menuJson)
