from dining import secure

def backup(s3, key, backupFolder):
    bucket = s3.Bucket(secure.S3['bucket'])
    objs = list(bucket.objects.filter(Prefix=key))
    if len(objs) > 0 and objs[0].key == key:
        bucket.copy({
            'Bucket': secure.S3['bucket'],
            'Key': key
        }, backupFolder + '/' + key)

        #data = bucket.Object(key).get()['Body'].read().decode("utf-8")
        #bucket.put_object(Key=backupFolder + '/' + key, Body=data)
    else:
        print("Note: " + key + " does not currently exist in this bucket.")
    return
