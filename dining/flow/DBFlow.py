import pymssql
import json
from dining import secure
import boto3
import os
from jsonschema import validate
from jsonschema.exceptions import ValidationError

from dining import settings
from dining import Mail
from dining.flow import S3

class DBFlow:
    def __init__(self, secure=secure, db=pymssql):
        self.server = secure.DB['server']
        self.port = secure.DB['port']
        self.db = secure.DB['db']
        self.user = secure.DB['user']
        self.password = secure.DB['password']
        try:
            self.conn = db.connect(host=self.server, user=self.user, password=self.password, database=self.db, port=self.port)
        except pymssql.DatabaseError as e:
            Mail.send("Database Error", "An Error with connecting to the database: " + str(e))
            sys.exit(1)

        try:
            if 'access_id' in secure.S3:
                self.s3 = boto3.resource(service_name='s3', aws_access_key_id=secure.S3['access_id'], aws_secret_access_key=secure.S3['secret_key'], region_name=secure.S3['region'])
            else:
                self.s3 = boto3.resource(service_name='s3', region_name=secure.S3['region'])
            self.bucket = secure.S3['bucket']
        except Exception as e:
            Mail.send("S3 Error", "An error with the s3 resource: " + str(e))
            sys.exit(1)

    def locations(self, create_file=False):
        cursor = self.conn.cursor()
        cursor.execute("""
        select location_number, location_name, address, city, state, zip_code
        from Locations
        """)
        row = cursor.fetchone()
        locations = []
        while row:
            locations.append({'id': row[0],
                'name': row[1],
                'address': row[2],
                'city': row[3],
                'state': row[4],
                'zip': row[5]
            })
            row = cursor.fetchone()

        schemaFile = open(settings.BASE_DIR + '/dining/schemas/locations.schema.json')
        schema = json.load(schemaFile)

        try:
            validate(locations, schema)
        except ValidationError as e:
            Mail.send("Invalid json", "JSON Generated (locations) does not conform to the json schema: " + str(e) + '\n\n' + json.dumps(locations))
            sys.exit(1)

        if create_file:
            #f = open('locations.json', 'w')
            #json.dump(locations, f)
            S3.backup(self.s3, 'locations.json', 'backup')

            locationJson = json.dumps(locations)
            self.s3.Bucket(self.bucket).put_object(Key='locations.json', Body=locationJson)

        return locations

    def recipes(self, create_file=False):
        cursor = self.conn.cursor()
        cursor.execute("""
        select
        ID,
        Serve_Date,
        Meal_Number,
        Meal_Name,
        Location_Number,
        Location_Name,
        Menu_Category_Number,
        Menu_Category_Name,
        Recipe_Number,
        Recipe_Name,
        Recipe_Print_As_Name,
        Ingredient_List,
        Allergens,
        Recipe_Print_As_Color,
        Recipe_Print_As_Character,
        Recipe_Product_Information,
        convert(varchar(30), Selling_Price, 1) as selling_price,
        convert(varchar(30), Portion_Cost, 1) as portion_cost,
        Production_Department,
        Service_Department,
        Catering_Department,
        Recipe_Web_Codes,
        Serving_Size,
        Calories,
        Calories_From_Fat,
        Total_Fat,
        Total_Fat_DV,
        Sat_Fat,
        Sat_Fat_DV,
        Trans_Fat,
        Trans_Fat_DV,
        Cholesterol,
        Cholesterol_DV,
        Sodium,
        Sodium_DV,
        Total_Carb,
        Total_Carb_DV,
        Dietary_Fiber,
        Dietary_Fiber_DV,
        Sugars,
        Sugars_DV,
        Protein,
        Protein_DV,
        Update_Date
        from ForecastedRecipes
        where production_department <> '97'
        """)

        row = cursor.fetchone()
        recipes = []
        while row:
            recipes.append({
                'id': row[0],
                'serve_date': row[1],
                'meal_id': row[2],
                'meal_name': row[3],
                'location_id': row[4],
                'location_name': row[5],
                'menu_category_number': row[6],
                'menu_category_name': row[7],
                'recipe_number': row[8],
                'recipe_name': row[9],
                'recipe_print_name': row[10],
                'ingredient_list': row[11],
                'allergens': row[12],
                'recipe_print_color': row[13],
                'recipe_print_char': row[14],
                'recipe_info': row[15],
                'selling_price': row[16],
                'portion_cost': row[17],
                'production_department': row[18],
                'service_department': row[19],
                'catering_department': row[20],
                'recipe_web_codes': row[21],
                'serving_size': row[22],
                'calories': row[23],
                'calories_from_fat': row[24],
                'total_fat': row[25],
                'total_fat_dv': row[26],
                'sat_fat': row[27],
                'sat_fat_dv': row[28],
                'trans_fat': row[29],
                'trans_fat_dv': row[30],
                'cholesterol': row[31],
                'cholesterol_dv': row[32],
                'sodium': row[33],
                'sodium_dv': row[34],
                'total_carb': row[35],
                'total_carb_dv': row[36],
                'dietary_fiber': row[37],
                'dietary_fiber_dv': row[38],
                'sugars': row[39],
                'sugars_dv': row[40],
                'protein': row[41],
                'protein_dv': row[42],
                'update_date': row[43]
            })
            row = cursor.fetchone()

        schemaFile = open(settings.BASE_DIR + '/dining/schemas/recipes.schema.json')
        schema = json.load(schemaFile)

        try:
            validate(recipes, schema)
        except ValidationError as e:
            Mail.send("Invalid json", "JSON Generated (recipes) does not conform to the json schema: " + str(e) + '\n\n' + json.dumps(recipes))
            sys.exit(1)

        #f = open('recipes.json', 'w')
        #json.dump(recipes, f)
        if create_file:
            S3.backup(self.s3, 'recipes.json', 'backup')

            recipeJson = json.dumps(recipes)
            self.s3.Bucket(self.bucket).put_object(Key='recipes.json', Body=recipeJson)

        return recipes

    def events(self, create_file=False):
        cursor = self.conn.cursor()

        with open(settings.BASE_DIR + '/dining/flow/eventTimes.json') as data_file:
            locationEventTimeJson = json.load(data_file)

        cursor.execute("""
        select menu_category_name,
            location_number,
            location_name,
            serve_date,
            menu_category_number
        from forecastedrecipes
        where production_department <> '97'
        group by menu_category_name, location_number, location_name, serve_date, menu_category_number
        order by location_number;
        """)
        row = cursor.fetchone()
        events = []

        while row:
            eventDict = {
                'menu_category_name': row[0],
                'location_id': row[1],
                'location_name': row[2],
                'serve_date': row[3],
                'menu_category_id': row[4]
            }
            startTime = ''
            endTime = ''
            for locationEventTimes in locationEventTimeJson:
                if locationEventTimes['id'] == str(eventDict['location_id']):
                    for eventTime in locationEventTimes['events']:
                        if eventTime['name'] in eventDict['menu_category_name']:
                            eventDict['start_time'] = eventTime['startTime']
                            eventDict['end_time'] = eventTime['endTime']

            events.append(eventDict)
            row = cursor.fetchone()

        schemaFile = open(settings.BASE_DIR + '/dining/schemas/events.schema.json')
        schema = json.load(schemaFile)

        try:
            validate(events, schema)
        except ValidationError as e:
            Mail.send("Invalid json", "JSON Generated (events) does not conform to the json schema: " + str(e) + '\n\n' + json.dumps(events))
            sys.exit(1)

        if create_file:
            S3.backup(self.s3, 'modo-events.json', 'backup')

            eventJson = json.dumps(events)
            self.s3.Bucket(self.bucket).put_object(Key='modo-events.json', Body=eventJson)

        return events

    def close(self):
        self.conn.close()
