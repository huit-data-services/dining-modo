from django.test import TestCase
from dining import settings
import json
from unittest import mock
from dining.flow.DBFlow import DBFlow
from dining.flow.ModoFlow import ModoFlow
import jsonschema
from jsonschema import validate
from jsonschema.exceptions import ValidationError

class FakeCursor():
    def __init__(self):
        self.stuff = []
    def execute(self, sql):
        junkList = []
        for i in range(0, 100):
            junkList.append("value")
        for i in range(0, 100):
            self.stuff.append(junkList)
    def fetchone(self):
        if len(self.stuff) > 0:
            return self.stuff.pop()
        else:
            return False
class FakeConn():
    def __init__(self):
        self.cursor = FakeCursor
    def cursor(self):
        return self.cursor
class FakeDb():
    def __init__(self):
        self.conn = FakeConn()
    def connect(self, host, user, password, database, port):
        return self.conn

class DBFlowTestCase(TestCase):
    def setUp(self):
        self.dbFlow = DBFlow(db=FakeDb())

    def test_locations_returns_correct_format(self):
        locations = self.dbFlow.locations()
        self.assertTrue(type(locations) is list)
        self.assertTrue('id' in locations[0])
        self.assertTrue('name' in locations[0])
    def test_locations_validates(self):
        locations = self.dbFlow.locations()
        schemaFile = open(settings.BASE_DIR + '/dining/schemas/locations.schema.json')
        schema = json.load(schemaFile)

        locations[0]['id'] = 1
        with self.assertRaises(ValidationError) as context:
            validate(locations, schema)
        locations[0]['id'] = "1"
        try:
            validate(locations, schema)
        except Exception as e:
            self.fail("Validation Error: " + str(e))

    def test_recipes_validates(self):
        recipes = self.dbFlow.recipes()
        schemaFile = open(settings.BASE_DIR + '/dining/schemas/recipes.schema.json')
        schema = json.load(schemaFile)



class ModoFlowTestCase(TestCase):
    def setUp(self):
        self.modoFlow = ModoFlow()
        self.recipes = [
            {
                'id': 1,
                'name': "test name 1",
                'description': "test description 1",
                'ingredient_list': "test igredient list 1",
                'event_id': "test event_id 1",
                'serve_date': "12/12/2012",
                'location_id': "01",
                'recipe_print_name': "recipe print name 1",
                'recipe_info': "some recipe info",
                'menu_category_number': "11",
                'allergens': "one",
                'recipe_web_codes': "AS",
                'serving_size': "1 cup",
                'calories': "1",
                'calories_from_fat': "10",
                'total_carb': "1",
                'dietary_fiber': "6",
                'sugars': "1",
                'sodium': "1",
                'trans_fat': "1",
                'sat_fat': "1",
                'cholesterol': "1",
                'meal_id': 12
            },
            {
                'id': 2,
                'name': "test name 2",
                'description': "test description 2",
                'ingredient_list': "test igredient list 2",
                'event_id': "test event_id 2",
                'serve_date': "12/12/2013",
                'location_id': "02",
                'recipe_print_name': "recipe print name 2",
                'recipe_info': "some recipe info",
                'menu_category_number': "22",
                'allergens': "one two",
                'recipe_web_codes': "DF AS",
                'serving_size': "1g",
                'calories': "200",
                'calories_from_fat': "200",
                'total_carb': "2",
                'dietary_fiber': "1",
                'sugars': "18",
                'sodium': "10",
                'trans_fat': "1",
                'sat_fat': "1",
                'cholesterol': "1",
                'meal_id': 13
            }
        ]

    def test_transform_menus_strips_nameless_entries(self):
        self.recipes[0]['name'] = '';
        menus = self.modoFlow.transformMenus(self.recipes)
        self.assertEquals(1, len(menus))
