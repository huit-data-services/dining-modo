from django.core.management.base import BaseCommand, CommandError
from dining.flow.DBFlow import DBFlow
from dining.flow.ModoFlow import ModoFlow

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--nodb',
            action="store_true",
            dest="nodb",
            default=False,
            help="avoids regenerating the raw json from db"
        )
        parser.add_argument(
            '--nocreate',
            action="store_true",
            dest="nocreate",
            default=False,
            help="doesn't create the files"
        )
    def handle(self, *args, **options):
        dbflow = DBFlow()
        print("locations")
        locations = dbflow.locations(create_file=False)
        print("recipes")
        recipes = dbflow.recipes(create_file=False)
        print("events")
        events = dbflow.events(create_file=False)

        modoFlow = ModoFlow()
        print("modo-events")
        modoFlow.setEvents(events)
        print("modo-locations")
        modoFlow.locations(locations)
        print("modo-menus")
        modoFlow.menus(recipes)

        print("FIN")
        return
