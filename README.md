# Table of Contents
1. [Installation](#installation)
2. [API Documentation](#api-documentation)
3. [Modo Documentation](#modo-documentation)

## Installation

This is a django rest application. Install directions follow.

```
git clone <this repo>
cd <this repo dir>
virtualenv env
source env/bin/activate
```

One of the requirements: [pymssql](http://pymssql.org/en/stable/), which is a db connection lib for SQL Server, requires [FreeTDS](http://pymssql.org/en/latest/freetds.html) -- which is an non-pip install.

```
pip install -r requirements.txt
```

First create the secure.py file:
```
cp dining/secure.py.example dining/secure.py
```

To run the tests:
```
python manage.py test
```

To run the API service:
```
gunicorn --bind 0.0.0.0/5678 dining.wsgi:application
```

To run the flow that will populate data:
```
python manage.py flow
```

## API Documentation

Because this is django rest, the API is somewhat self documenting. Just go to the root url once it's running, e.g. `http://localhost:5678` and browse the routes.

## Raw Dining Data

There are two root level routes for getting the raw data directly from the dining database. Please note it is cached, it does not actually hit the db with a request.

/locations
/recipes

## Modo Documentation

There are three routes for the modo dining app. These are all composed from the raw data.

/locations
/menus
/menuItems
/events

There are also the ICS files, each location has an ICS file which can be accessed from the location route. The modo app itself (that this was build to interface with) is documented here:
- [Creating a Dining Locations Data Source](https://support.modolabs.com/support/solutions/articles/5000638490)
- [Adding Menu Information for the Dining Module](https://support.modolabs.com/support/solutions/articles/5000638508)
